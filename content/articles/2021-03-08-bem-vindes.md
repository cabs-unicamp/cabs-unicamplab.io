title: Bem vindes à Unicamp!
date: 2021-03-08
tags: ingressantes, pv
summary: Olá bixetes, bixes e bixos parabéns por conseguirem entrar na melhor universidade da América Latina, apesar do momento único que estamos vivendo. O CABS deseja tudo de bom nessa nova fase e que vocês consigam aproveitar o máximo dela. Caso precisem de alguma coisa ou estejam com alguma dúvida, não hesite em perguntar, iremos fazer o máximo para conseguir te ajudar! Bem vindes e beijão do tio Ber! Abaixo está o manual do ingressante preparado com muito carinho pelo Centro Acadêmico do seu curso. Não deixe de conferir!

<div style="text-align: center;">
<img src="/images/bem-vindes.png" alt="Bem vindes!" style="max-width: 80%;"/>
</div>

 Olá bixetes, bixes e bixos parabéns por conseguirem entrar na melhor universidade da América Latina, apesar do momento único que estamos vivendo. O CABS deseja tudo de bom nessa nova fase e que vocês consigam aproveitar o máximo dela. Caso precisem de alguma coisa ou estejam com alguma dúvida, não hesite em perguntar, iremos fazer o máximo para conseguir te ajudar! Bem vindes e beijão do tio Ber!

Abaixo está o manual do ingressante preparado com muito carinho pelo Centro Acadêmico do seu curso. Não deixe de conferir!

<div style="text-align: center;">
<a href="/pv/ingresso_2021.pdf"><img src="/pv/ingresso_2021.png" alt="Ponto de vista de ingresso 2021" style="display: inline-block; max-width: 300px;"/></a>
</div>
