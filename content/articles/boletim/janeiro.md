title: Boletim informativo: janeiro de 2021
date: 2021-01-21
tags: boletim

Olá, com o intuito de informar de forma rápida e breve sobre o que está acontecendo nas instâncias da FEEC e o outras questões importantes que podem passar despercebidas nesse momento de ensino remoto compulsório, o CABS divulga seu segundo boletim informativo.

Sabemos que é muito difícil fazer e acompanhar movimentação política “à distância”, esperamos conseguir usar este instrumento para levantar debates importantes que estão passando de forma invisível por conta da conjuntura.

Enfatizamos que o mais importante para que qualquer coisa aconteça é o envolvimento de mais pessoas, então convidamos todas e todos para nos contactar seja para contribuir com material para o boletim, ajudar em alguma pauta específica, denunciar problemas com o ensino remoto, ou conhecer mais o centro acadêmico. Para se comunicar, pode nos deixar mensagem no facebook, na sala pública no element/telegram ou enviar um email para geral.cabs@gmail.com.

<div class="boletim">cabs</div>

No começo do semestre passado, o CABS elaborou uma proposta de aulas para o 2º semestre de 2020 da graduação, que apesar de não ter sido votado/aprovado pela diretoria/coordenação de graduação, fizemos reuniões com os departamento que aceitaram discutir sobre (DSEE e DECOM) e assim, conseguimos levar as demandas relatadas ao CABS para a maior parte dos docentes. 

Além disso, com o objetivo de discutir mais frequentemente problemas do ensino remoto e da faculdade em geral, foi proposto pela coordenação que nos reuníssemos regularmente. Desde então, temos nos reunido quinzenalmente com o coordenador de curso da engenharia elétrica (Prof. Leandro Tiago Manera) e discutido alguns pontos que foram levantados pelos estudantes. Caso tenha alguma duvida/reclamação entre em contato com o CABS, ainda estamos recebendo reclamações sobre o ensino remoto emergencial e levando à coordenação por meio dessas reuniões e demais órgãos.

A partir das discussões realizadas na avaliação de curso tivemos algumas propostas levantadas que estamos encaminhando junto à coordenação. No tópico Avaliação de Curso iremos detalhar melhor.

No começo de dezembro de 2020 a coordenação junto com o CABS e Centro Acadêmico da Computação (CACo) publicou um formulário de levantamento da demanda de matérias de ferias de verão. Apesar da decisão da coordenação de não realizar matérias de férias, várias reclamações, dúvidas e sugestões relatadas no formulário pelos estudantes estão sendo levadas à coordenação e outros órgãos.

<div class="boletim">congregação</div>

A congregação é o órgão máximo de deliberação na FEEC. As reuniões acontecem na última segunda feira de cada mês. Ela conta com representantes discentes, docentes e de funcionários, além dos diretores, coordenadores da graduação e coordenador de extensão. As pautas e atas ficam registradas no seguinte link, no site da feec: <https://www.fee.unicamp.br/node/241>.

Desde o último boletim, vale destacar entre as pautas a alteração do Regimento Interno da FEEC. Dois pontos foram alterados: a distribuição de departamentos e a composição da CG (Comissão de Graduação). No que se refere ao primeiro ponto, o DEB (Departamento de Engenharia Biomédica), foi extinto. A Engenharia Biomédica é uma das certificações possíveis na FEEC. Além disso, os pesquisadores e pesquisadoras dessa área desenvolveram várias pesquisas voltadas para a COVID-19. Esse corte de departamentos não ocorre apenas na FEEC, mas em vários institutos da Unicamp. Esse quadro é preocupante, uma vez que indica uma redução do potencial da universidade. Menos professores são contratados, existem menos departamentos e áreas de pesquisa, há menos investimento no instituto. Está vigente entre as universidades paulistas um congelamento de novas contratações. 

Em relação à composição da CG, houve duas propostas: uma previa adicionar à composição atual um funcionário que não fosse docente e a outra previa reduzir a representação discente. Não havia até então nenhum funcionário que não fosse da categoria docente, apesar de serem os maiores responsáveis por dar suporte para o funcionamento da FEEC. Em relação à representação discente, existem dois alunos da engenharia de computação e dois da engenharia elétrica. Os alunos são os responsáveis por levar o ponto de vista dos discentes para as discussões e apresentar necessidades e dificuldades. Embora tenham poder de voto, são minoria dentro da composição da CG. Assim, diminuir a quantidade de representantes significaria diminuir ainda mais a pequena representatividade que temos enquanto categoria. Por fim, foi aprovado incluir um funcionário e a representação discente foi mantida da forma atual.

Outra pauta importante foi a implementação do vestibular indígena na engenharia da computação. Até então, ele existia apenas na engenharia elétrica. A congregação aprovou também um voto favorável na CCG na discussão de uma nova grade para estudantes indígenas. A partir de agora, no primeiro semestre, ingressantes indígenas irão cursar um núcleo básico de disciplinas e, a partir do segundo semestre, passarão a acompanhar a grade do curso em que entraram. Essa proposta foi elaborada pelo próprio coletivo dos Acadêmicos Indígenas.

Diante da alteração para a grade curricular de engenharia e também dos cursos de elétrica e computação pelo MEC, estão sendo pensadas formas de inserir essas diretrizes em nossos cursos. A discussão ainda está em andamento. 

A próxima Congregação será dia 25 de janeiro. Em fevereiro, haverá recesso e as reuniões serão retomadas em março. Os informes serão postados posteriormente pelo CABS.

<div class="boletim">comissão de graduação</div>

A Comissão de Graduação (CG) tem a competência de supervisionar, administrar e coordenar todas as atividades relativas aos cursos de Graduação em Engenharia Elétrica e Engenharia de Computação. Seus membros são os coordenadores da engenharia elétrica e de computação, um representante de cada departamento da FEEC e representantes discentes. As reuniões acontecem na primeira segunda-feira de cada mês e as pautas e atas podem ser encontradas em: <https://www.fee.unicamp.br/node/483>.

Na CG houve várias discussões sobre as aulas de férias de verão e do 1º semestre/2021. Com relação às matérias de férias, realizamos um levantamento de interesse dos alunos através de um formulário, mas apesar do expressivo número de respostas a coordenação optou por não oferecer as disciplinas, alegando que o prazo para envio de kits de laboratório e matrícula era muito curto. Foi dito que haveria turmas o suficiente para dar conta dessa demanda no semestre regular, mas caso tenham dificuldades de conseguir vagas nas disciplinas, especialmente em laboratórios, entrem em contato com o CABS.

Para o 1º semestre/2021, a orientação da reitoria é que disciplinas teóricas sejam oferecidas de forma totalmente remota e atividades práticas sejam feitas presencialmente apenas se for indispensável. Com isso, na FEEC a única disciplina que poderá ser presencial é EE103, para a qual está sendo proposto um oferecimento híbrido em que os alunos matriculados teriam que ir à FEEC pelo menos duas vezes no semestre. O CABS foi contrário à realização de atividades presenciais, considerando os diversos problemas que podem ser acarretados pela exigência de alunos frequentando o campus, e não recomenda a matrícula na disciplina.
Defendemos porém a suspensão dos pré-requisitos que exigem EE103, conforme flexibilização aprovada pela CEPE para este período de ensino remoto, no entanto nossa proposta não foi implementada.

Outro assunto abordado foram os resultados do último ENADE feito pelos alunos da FEEC, em que um fator que chamou a atenção foi a avaliação negativa que os alunos fizeram da formação integral, ética e cidadã que recebem do curso, o que suscitou o debate do tema "Ética Universitária" na reunião de avaliação de curso.

Devido ao recesso de janeiro, a próxima CG será dia 01 de fevereiro. Os informes serão postados posteriormente pelo CABS.

<div class="boletim">reunião de avaliação de curso</div>

Tema: Ética na Universidade

O tema da avaliação de curso do segundo semestre de 2020 foi Ética na Universidade. A motivação do tema foi a falta de discussão dessas questões dentro do nosso curso. Um indicativo disso foi o resultado das respostas no ENADE (Exame Nacional de Desempenho de Estudantes), que é uma prova realizada por estudantes de ensino superior no final do curso. Ao fim da prova, existe uma série de perguntas sobre a impressão dos alunos sobre conteúdos abordados na universidade. O resultado relativo à FEEC foi que a grande maioria dos estudantes não sentiram que o curso discutia o suficiente questões éticas e sociais. 

Além disso, diante da forma que tem ocorrido o ensino remoto emergencial, a pauta também pareceu necessária. Ao longo dos dois semestres do ano anterior, na avaliação de curso do primeiro semestre e entre relatos recebidos pelo email do CABS, os alunos têm relatado dificuldade no diálogo com professores, sobrecarga de trabalhos e de dedicação ao estudo e também dificuldade em se sentirem ouvidos pelos professores. Em relação aos professores, eles têm relatado fraudes nas provas e alguns exigiram que os alunos fizessem prova oral ou que realizassem as provas escritas com a câmera ligada. Avaliamos que a sobrecarga, somada às dificuldades de diálogo, tem gerado essas problemáticas. 

Durante a discussão, os alunos retomaram o que foi dito sobre diálogo e sobrecarga. Além disso, também falaram que as discussões éticas de fato não estão presentes no curso, que se mostra apenas técnico. A falta de discussão sobre questões raciais e de gênero, sobretudo em um curso com maioria de homens e pessoas brancas, também foi apontada. 

Os professores presentes relataram que não se sentem preparados para discutir essas questões dentro de suas disciplinas. Falta formação e também falta tempo para se formar. Com a carga de disciplinas e de pesquisa, os professores também estão sobrecarregados. Devido ao congelamento das contratações da universidade, não é possível também trazer novos professores que tenham essa formação. 

Diante disso, foi discutido que algumas possibilidades são que a comunidade da FEEC compareça mais em atividades promovidas por coletivos de mulheres e pessoas negras, que fazem diversos eventos para discutir essas temáticas na universidade. Além disso, foi falado sobre estimular debates e trazer convidados para as aulas que falem sobre temas relativos a gênero, raça e ética, entre outras questões sociais. 

Como encaminhamento, o CABS, junto ao Engenho Negro (coletivo negro das engenharias) e à Semana das Minas (coletivo feminista), estão idealizando a realização de um evento e de uma disciplina voltados a discutir questões sociais dentro das engenharias. Estamos nos reunindo com a coordenação para discutir a execução desses projetos. Se tiverem dúvidas, sugestões ou quiserem participar, mandem email para o CABS!

<div class="boletim">comunidade externa</div>

Como foi dito no boletim anterior, permanecem as reuniões entre os centros acadêmicos da universidade e o DCE (Diretório Central dos Estudantes) a fim de discutir pautas sobre a realidade dos institutos e da universidade como um todo nesse período.

Além disso, as bolsas SAE continuam sendo pautadas nos espaços institucionais e no movimento estudantil. Lembrando que houve uma proposta na CEPE (Câmara de Ensino Pesquisa e Extensão) que atribuía critérios meritocráticos para bolsas que são sociais e que dava margem para o corte de bolsas. Os estudantes se organizaram em três GTs (Grupos de Trabalho): o de pesquisa, que visava recolher dados sobre a realidade dos bolsistas e sustentar uma nova proposta sobre a atribuição de bolsas; o de mobilização, que visava divulgar a pauta para a comunidade estudantil via reuniões e redes sociais, além de outras formas de intervenção; e o de contra-proposta, que visava escrever uma nova proposta de atribuição de bolsas, com base nos dados obtidos com o GT de pesquisa e consultas à comunidade discente. Também foi criado um GT institucional, oficializado pela reitoria, com a presença de docentes. Houve uma grande vitória com relação a essa pauta, uma vez que a reitoria atual se comprometeu a não encaminhar a votação da proposta da reitoria nessa gestão.

Apesar da vitória obtida por meio da mobilização estudantil em torno da pauta das bolsas SAE, ela só está garantida durante a gestão atual da reitoria. Contudo, como ela está relacionada a diversos cortes que a Unicamp tem sofrido, os quais geralmente afetam mais as ações de permanência, a mobilização ainda é necessária. Os GTs continuarão se reunindo para elaborar uma proposta que de fato contemple estudantes e quem desejar compor algum deles pode mandar mensagem para a página Bolsistas SAE em Luta, no instagram e facebook.

Vale lembrar que no ano que vem ocorrem as eleições para a reitoria, sendo o primeiro turno no dia 12/03/2021. Já existem três chapas candidatas e têm ocorrido debates com vários grupos. A eleição é feita por meio da formação de uma lista tríplice, a qual é enviada ao governo de São Paulo, sendo que o governador escolhe dentro da lista tríplice quem será o novo reitor. É importante participar dos debates e conhecer as propostas das três chapas, se atentando a suas prioridades e propostas para a Unicamp, especialmente em um cenário de ataques à ciência e à universidade.

Por fim, tem sido discutido no CRU (Conselho dos Representantes de Unidade) como deve ser encaminhada a próxima eleição para o DCE. A gestão deveria ter acabado por volta de novembro, sendo convocadas novas eleições. No geral, as eleições para o DCE ocorrem por votação em urnas, organizadas por uma Comissão Eleitoral, sendo que cada instituto tem direito a enviar pessoas para compor essa comissão, que podem tanto ser eleitas quanto referendadas pelo centro acadêmico. Porém, com a pandemia e a suspensão de atividades presenciais, a realização de eleições presenciais é uma dificuldade que se estende também para os centros acadêmicos. Foi feita uma assembleia para que essa questão pudesse ser discutida. Como ela ficou esvaziada (cerca de apenas 40 estudantes), o DCE encerrou a assembleia. Vale destacar que, em geral, quando as assembleias ficam muito esvaziadas, é comum optar por fazer uma plenária, com uma consulta a estudantes presentes sobre opiniões e possibilidades. Contudo, o DCE apenas encerrou a assembleia e não deu margem para essa opção. Foi realizado um CRU no dia 16 de dezembro, o último do ano, e optou-se por prolongar a gestão até janeiro, para que possamos pensar alternativas e possibilidades para as eleições nesse contexto.

Em janeiro, o CRU decidiu por manter a gestão até a calourada. No final de fevereiro será feita uma reunião para decidir como será a calourada e para construir uma assembleia para discutir como ficará a gestão do DCE durante a pandemia. 

Por fim, com a aprovação pela ANVISA do uso emergencial da CORONAVAC, a Unicamp montou um calendário de vacinação que deve acompanhar o calendário previsto para o estado de São Paulo. 
