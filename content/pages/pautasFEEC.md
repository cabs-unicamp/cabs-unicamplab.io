title: Pautas (FEEC)
slug: pautas-feec
---

Pautas e atas completas das reuniões da Faculdade de Engenharia Elétrica e de Computação da Unicamp

## Setembro

- [Congregação](/pautas/set-congregacao.pdf)
- [Congregação (suplementar)](/pautas/set-congregacao-sup.pdf)
- [Comissão de Graduação](/pautas/set-cg.pdf)
- [Comissão de Graduação (suplementar)](/pautas/set-cg-sup.pdf)
- [Conselho Interdepartamental](/pautas/set-ci.pdf)
- [Conselho Interdepartamental (suplementar)](/pautas/set-ci-sup.pdf)
