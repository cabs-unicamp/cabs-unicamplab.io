title: Ponto de Vista
template: pv


## Cada um tem seu ponto, conheça o nosso!

O jornal Ponto de Vista existe desde o início da década de 80 e se caracterizava como um jornal de opinião para discussões políticas e relacionadas a engenharia. A partir de 2013 sua estrutura tem focado em garantir um espaço para que estudantes se expressem política e culturalmente. Hoje, nosso jornal se caracteriza como um meio livre de comunicação entre membras e membros da nossa comunidade acadêmica.

## Contribua com material

Para fazer do seu ponto parte do Nosso Ponto, envie seus textos para [pvjornal&#46;cabs&#64;gmail&#46;com](mailto:pvjornal&#46;cabs&#64;gmail&#46;com). Tome também alguns segundos para ler nossas principais diretrizes:

  - Não aceitamos conteúdo com caráter ofensivo
  - O material pode ser publicado de forma autoral ou anônima (explicite no email)
  - Temos como principais objetivos:
    - Salientar os tópicos de discussões políticas de nível nacional a local
    - Lutar contra todas as formas de opressão
    - Promover a expressão artístico-cultural (aceitamos, desenhos, fotos, textos, etc.)
    - Trazer quaisquer discussões pertinentes a nossa comunidade acadêmica

Distribuímos nosso jornal em versão impressa em alguns pontos da Unicamp, e em versão digital na nossa página do Facebook e nesse site.
Nosso conteúdo é protegido por licença Creative Commons [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) (até 2019 [CC BY-ND 3.0](https://creativecommons.org/licenses/by-nd/3.0/)).
